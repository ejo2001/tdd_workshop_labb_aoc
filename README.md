# TDD

TDD is pretty straight forward. You write a test for how you want the code to behave, and that's it. It is a bit tougher to read from a structural standpoint, as well as write as it can get quite hard to keep track of what you are doing compared to BDD. I enjoyed working with TDD as it didn't add a lot of files and made testing much more straightforward for the assignments I had. 
