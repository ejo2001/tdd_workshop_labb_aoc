def smallest_value(values):
	smallest = values[0]
	for i in values:
		if i < smallest:
			smallest = i
	return smallest


def wrapping_paper(inp):
	l, w, h = inp.split("x")
	length_x_width = int(l)*int(w)
	width_x_height = int(w)*int(h)
	height_x_length = int(h)*int(l)
	value_list = [length_x_width, width_x_height, height_x_length]
	smallest = smallest_value(value_list)
	paper_size = (2*(length_x_width)) + (2*(width_x_height)) + (2*(height_x_length)) + smallest
	return paper_size

def tot_paper(inp):
	tot = 0
	for i in inp:
		tot += wrapping_paper(i)
			
	return tot



#with open('math_input') as file:
#	inp = []
#	for line in file:
#		inp.append(line)
#	print(tot_paper(inp))
