import day2_assignment1 as main

tests = ["2x3x4", "1x1x10"]
results = [58, 43]

def test_paper_size():
	for result, test in enumerate(tests):
		assert main.wrapping_paper(test) == results[result]

def test_tot_script():
	assert main.tot_paper(tests) == 101
