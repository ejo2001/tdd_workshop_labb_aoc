import day2_assignment2 as main

tests = ["2x3x4", "1x1x10"]
results = [34, 14]

def test_ribbon_size():
	for result, test in enumerate(tests):
		assert main.ribbon_area(test) == results[result]

def test_tot_size():
	assert main.ribbon_size(tests) == 48
