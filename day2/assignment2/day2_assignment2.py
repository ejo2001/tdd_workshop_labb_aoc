def ribbon_area(inp):
	l, w, h = inp.split("x")
	l, w, h = int(l), int(w), int(h)
	ribbon_size = 2 * min(l+w, w+h, h+l) 
	ribbon_size += l*w*h
	return ribbon_size

def ribbon_size(inp):
	tot = 0
	for i in inp:
		tot += ribbon_area(i)
			
	return tot




#with open('math_input') as file:
#	inp = []
#	for line in file:
#		inp.append(line)
#	print("My answer: " + str(ribbon_size(inp)))
