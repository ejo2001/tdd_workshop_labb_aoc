import main2 as floor_code

tests = [")", "(()))", "()()())"]
results = [1, 5, 7]

def test_output():
	for result, test in enumerate(tests):
		assert floor_code.first_basement(test) == results[result] 
