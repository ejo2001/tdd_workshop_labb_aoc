import main1 as floor_code

example = "()(("

tests = ["()", "(((", "()()("]
results = [0, 3, 1]

def test_output():
	for result, test in enumerate(tests):
		assert floor_code.count_floors(test) == results[result] 
